﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Ecopawz2
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "Home/{id}",
                defaults: new { controller = "Home", action = "Index", id = "Index" }
            );

            routes.MapRoute(
                name: "Pages",
                url: "{id}",
                defaults: new { controller = "Home", action = "Index", id = "Index" }
            );
        }
    }
}