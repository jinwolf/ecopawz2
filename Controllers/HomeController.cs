﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Ecopawz2.Controllers
{
    public class HomeController : Controller
    {    
        public ActionResult Index(string id)
        {
            ViewBag.ShowConfirmMessage = "";

            if (id.Equals("ContactUs", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(Request.Form["Email"]))
            {
                SendEmail(Request.Form["Email"]);
                ViewBag.ShowConfirmMessage = "True";
            }

            return View(id);
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="email">The email.</param>
        private void SendEmail(string email)
        {   
            SmtpClient smtpClient = new SmtpClient("mail.ecopawz.com", 25);

            smtpClient.Credentials = new System.Net.NetworkCredential("info@ecopawz.com", "myIDPassword");
            smtpClient.UseDefaultCredentials = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            MailMessage mail = new MailMessage();


            //Setting From , To and CC
            mail.From = new MailAddress("info@ecopawz.com", "Ecopawz Web Site");
            mail.To.Add(new MailAddress(email));
            smtpClient.Send(mail);                               
        }
    }
}
